import React from 'react';
import './App.css';
import { Container, Typography } from '@material-ui/core';
import Keys from './Keys';
import KeyPressHandler from './KeyPressHandler';

const App = () => {
    return (
        <Container className="App">
            <Typography variant="h3">
                Turn on your music and play along!
            </Typography>
            <KeyPressHandler>
                <Keys />
            </KeyPressHandler>
        </Container>
    );
}

export default App;
