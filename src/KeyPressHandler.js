const KeyPressHandler = (props) => {
    const eventHandler = (key) => {
        switch (key) {
            case 'q': {
                const audio = new Audio('samples/SmallCassa_2C.mp3');
                audio.play();
                break;
            }
            case 'w': {
                const audio = new Audio('samples/MadalDrum_2C.mp3');
                audio.play();
                break;
            }
            case 'e': {
                const audio = new Audio('samples/Darabuka_6E.mp3');
                audio.play();
                break;
            }
            case 'a': {
                const audio = new Audio('samples/Handdrum_4С.mp3');
                audio.play();
                break;
            }
            case 's': {
                const audio = new Audio('samples/Darabuka_1A.mp3');
                audio.play();
                break;
            }
            case 'd': {
                const audio = new Audio('samples/IranBong_4A.mp3');
                audio.play();
                break;
            }
            case 'z': {
                const audio = new Audio('samples/SynthHit_016.mp3');
                audio.play();
                break;
            }
            case 'x': {
                const audio = new Audio('samples/Dulcimer_3A.mp3');
                audio.play();
                break;
            }
            case 'c': {
                const audio = new Audio('samples/Sitar_1C.mp3');
                audio.play();
                break;
            }
            default:
                return;
        }
    };

    window.addEventListener('keydown', (event) => {
        const button = document.getElementById(event.key);
        if (button) {
            button.click();
            button.dispatchEvent(new Event('mousedown', {bubbles: true}));
            button.dispatchEvent(new Event('mouseup', {bubbles: true}));

        }
    })

    window.addEventListener('click', (event) => {
        eventHandler(event.target.innerText.toLowerCase());
    })

    return props.children;
};

export default KeyPressHandler;