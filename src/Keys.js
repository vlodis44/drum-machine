import React from 'react';
import { Container, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
    keyContainer: {
        borderRadius: '50%',
        marginTop: '10%',
        padding: '10% 0',
        background: 'white url("background.jpg") no-repeat scroll center center'
    }
})

const Keys = () => {
    const classes = useStyles();

    return (
        <Container className={classes.keyContainer}>
            <Container>
               <Button variant="contained" id="q" size="large">
                   q
               </Button>
               <Button variant="contained" id="w" size="large">
                   w
               </Button>
               <Button variant="contained" id="e" size="large">
                   e
               </Button>
            </Container>
            <Container>
                <Button variant="contained" id="a" size="large">
                    a
                </Button>
                <Button variant="contained" id="s" size="large">
                    s
                </Button>
                <Button variant="contained" id="d" size="large">
                    d
                </Button>
            </Container>
            <Container>
                <Button variant="contained" id="z" size="large">
                    z
                </Button>
                <Button variant="contained" id="x" size="large">
                    x
                </Button>
                <Button variant="contained" id="c" size="large">
                    c
                </Button>
            </Container>
        </Container>
    );
};

export default Keys;